package ro.ubb.catalog.core.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Book.class)
public abstract class Book_ extends ro.ubb.catalog.core.model.BaseEntity_ {

	public static volatile SingularAttribute<Book, String> IBAN;
	public static volatile ListAttribute<Book, Purchase> purchases;
	public static volatile SingularAttribute<Book, Double> price;
	public static volatile SingularAttribute<Book, String> author;
	public static volatile SingularAttribute<Book, String> title;

}

