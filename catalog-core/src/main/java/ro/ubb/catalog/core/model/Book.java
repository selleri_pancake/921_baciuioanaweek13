package ro.ubb.catalog.core.model;

import lombok.*;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by radu.
 */
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString(callSuper = true, exclude = "purchases")
@EqualsAndHashCode(callSuper = true, exclude = "purchases")
public class Book extends BaseEntity<Long> {

    @Column(name = "IBAN", nullable = false)
    private String IBAN;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "price", nullable = false)
    private Double price;
    @Column(name = "author", nullable = false)
    private String author;

    @OneToMany(mappedBy = "book",orphanRemoval = true, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Purchase> purchases = new ArrayList<>();

    public List<Client> getClients() {
        return Collections.unmodifiableList(
                purchases.stream()
                        .map(Purchase::getClient)
                        .collect(Collectors.toList())
        );
    }

    public void addClient(Client client) {
        Purchase purchase = new Purchase();
        purchase.setClient(client);
        purchase.setBook(this);
        purchases.add(purchase);
    }

    public void addPurchase(Client client, Date date) {
        Purchase purchase = new Purchase();
        purchase.setClient(client);
        purchase.setDate(date);
        purchase.setBook(this);
        purchases.add(purchase);
    }

    public void deletePurchase(Long id){
        purchases.removeIf(p->p.getClient().getId().equals(id));
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (!(o instanceof Book)) return false;
//        Book book = (Book) o;
//        return Objects.equals(getIBAN(), book.getIBAN()) &&
//                Objects.equals(getTitle(), book.getTitle()) &&
//                Objects.equals(getPrice(), book.getPrice()) &&
//                Objects.equals(getAuthor(), book.getAuthor()) ;
//    }

//    @Override
//    public int hashCode() {
//        return title.hashCode();
//    }
//
//    @Override
//    public String toString() {
//        return "Book{" +
//                "IBAN='" + IBAN + '\'' +
//                ", title='" + title + '\'' +
//                ", price=" + price +
//                ", author='" + author + '\'' +
//                ", studentDisciplines=" + purchases +
//                '}';
//    }
}
