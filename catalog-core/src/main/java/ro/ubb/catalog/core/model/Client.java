package ro.ubb.catalog.core.model;

import lombok.*;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;

//@NamedEntityGraph(attributeNodes = {
//        @NamedEntityGraph(name = "clientWithPurchases",
//                attributeNodes = @NamedAttributeNode(value = "purchases")),
//        @NamedEntityGraph(name = "clientWithPurchasesAndBook",
//                attributeNodes = @NamedAttributeNode(value = "purchases", subgraph = "purchaseWithBook"),
//                subgraphs = @NamedSubgraph(name = "purchaseWithBook",
//                        attributeNodes = @NamedAttributeNode(value = "book")))
//
//
//})

@NamedEntityGraphs({
        @NamedEntityGraph(name = "clientWithPurchases",
                attributeNodes = @NamedAttributeNode(value = "purchases")),
        @NamedEntityGraph(name = "clientWithPurchasesAndBook",
                attributeNodes = @NamedAttributeNode(value = "purchases", subgraph = "purchaseWithBook"),
                subgraphs = @NamedSubgraph(name = "purchaseWithBook",
                        attributeNodes = @NamedAttributeNode(value = "book")))
})



@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString(callSuper = true, exclude = "purchases")
@EqualsAndHashCode(callSuper = true, exclude = "purchases")
//@Table
//@SecondaryTable()
public class Client extends BaseEntity<Long> {


    @Column(name = "full_name", nullable = false)
    private String full_name;

    @Column(name = "address", nullable = false)
    private String address;


    //@ManyToMany
    @OneToMany(mappedBy = "client",orphanRemoval = true,cascade = CascadeType.ALL, fetch =
            FetchType.LAZY)
    private List<Purchase> purchases = new ArrayList<>();


    public List<Book> getBooks() {
        purchases = purchases == null ? new ArrayList<>() :
                purchases;
        return Collections.unmodifiableList(
                this.purchases.stream().
                        map(Purchase::getBook).
                        collect(Collectors.toList()));
    }

    public void addBook(Book book) {
        Purchase purchase = new Purchase();
        purchase.setBook(book);
        purchase.setClient(this);
        purchases.add(purchase);
    }

    public void addBooks(Set<Book> books) {
        books.forEach(this::addBook);
    }

    public void addPurchase(Book book, Date date) {
        Purchase purchase = new Purchase();
        purchase.setBook(book);
        purchase.setDate(date);
        purchase.setClient(this);
        purchases.add(purchase);
    }


    @Modifying
    @Transactional
    public void deletePurchase(Long id){
        purchases.removeIf(p->p.getBook().getId().equals(id));
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        Client client = (Client) o;
//
//        return full_name.equals(client.full_name) && address.equals(client.address);
//    }

//    @Override
//    public int hashCode() {
//        return full_name.hashCode();
//    }

//    @Override
//    public String toString() {
//        return "Client{" +
//                "full_name='" + full_name + '\'' +
//                ", address='" + address + '\'' +
//                ", studentDisciplines=" + purchases +
//                '}';
//    }
}
