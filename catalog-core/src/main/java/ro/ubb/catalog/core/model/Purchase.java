package ro.ubb.catalog.core.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by radu.
 */

@Entity
@Table(name = "purchase")
@IdClass(PurchasePK.class)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(exclude = {"book", "client"})
@Builder

public class Purchase implements Serializable {

    @Id
    @ManyToOne(optional = false,fetch = FetchType.LAZY)
    @JoinColumn(name = "client_id")
    private Client client;

    @Id
    @ManyToOne(optional = false , fetch = FetchType.LAZY)
    @JoinColumn(name = "book_id")
    private Book book;

    @Column(name = "purchase_date")
    private Date date;

    @Override
    public String toString() {
        return "Purchase{" +
//                "client=" + client.getId() +
//                ", book=" + book.getId() +
                ", date=" + date +
                '}';
    }
}
