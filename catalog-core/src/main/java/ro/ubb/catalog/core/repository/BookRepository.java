package ro.ubb.catalog.core.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.core.model.Client;

import java.util.List;

/**
 * Created by radu.
 */


public interface BookRepository extends CatalogRepository<Book, Long>, BookRepositoryCustom  {

    @Query("select distinct b from Book b")
    @EntityGraph(value = "clientWithPurchasesAndBook", type =
            EntityGraph.EntityGraphType.LOAD)
    List<Book> findAllWithPurchasesAndBook();

    @Query("select distinct b from Book b order by b.title")
    @EntityGraph(value = "clientWithPurchasesAndBook", type =
            EntityGraph.EntityGraphType.LOAD)
    List<Book> findAllWithPurchasesAndBookOrderByTitle();

}
