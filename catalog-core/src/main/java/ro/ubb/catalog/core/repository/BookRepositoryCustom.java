package ro.ubb.catalog.core.repository;

import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.core.model.Client;

import java.util.List;

public interface BookRepositoryCustom {

    List<Book> findAllWithPurchasesAndBookJPQL();
    List<Book> findAllWithPurchasesAndBookCriteriaAPI();
    List<Book> findAllWithPurchasesAndBookSQL();

    List<Book> findAllWithPurchasesAndBookSortedJPQL();
    List<Book> findAllWithPurchasesAndBookCriteriaSortedAPI();
    List<Book> findAllWithPurchasesAndBookSortedSQL();
}
