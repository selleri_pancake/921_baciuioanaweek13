package ro.ubb.catalog.core.repository;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.jpa.HibernateEntityManager;
import org.hibernate.query.criteria.internal.OrderImpl;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.catalog.core.model.*;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.*;
import java.util.List;

public class BookRepositoryImpl extends CustomRepositorySupport implements BookRepositoryCustom{
    @Override
    public List<Book> findAllWithPurchasesAndBookJPQL() {
        EntityManager entityManager = getEntityManager();
        Query query = entityManager.createQuery(
                "select distinct b from Book b " );

        List<Book> authors = query.getResultList();

        return authors;
    }

    @Override
    public List<Book> findAllWithPurchasesAndBookCriteriaAPI() {
        EntityManager entityManager = getEntityManager();

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Book> query = criteriaBuilder.createQuery(Book.class);
        query.distinct(Boolean.TRUE);
        Root<Book> root = query.from(Book.class);
//        query.select(root);
        Fetch<Book, Purchase> bookPurchaseFetch = root.fetch(Book_.purchases, JoinType.LEFT);
        bookPurchaseFetch.fetch(Purchase_.book, JoinType.LEFT);

        List<Book> authors = entityManager.createQuery(query).getResultList();

        return authors;
            }

    @Override
    @Transactional
    public List<Book> findAllWithPurchasesAndBookSQL() {
        HibernateEntityManager hibernateEntityManager = getEntityManager().unwrap(HibernateEntityManager.class);
        Session session = hibernateEntityManager.getSession();


        org.hibernate.Query query = session.createSQLQuery("select distinct {a.*},{b.*},{p.*} " +
                "from book a " +
                "left join purchase p on a.id=p.book_id " +
                "left join book b on p.book_id=b.id ")
                .addEntity("a",Book.class)
                .addJoin("p", "a.purchases")
                .addJoin("b", "p.book")
                .addEntity("a",Book.class)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        //todo aici nu stiu exavt cum sa fac, ca nu merge

        List<Book> authors = query.getResultList();


        return authors;     }

    @Override
    public List<Book> findAllWithPurchasesAndBookSortedJPQL() {
        EntityManager entityManager = getEntityManager();
        Query query = entityManager.createQuery(
                "select distinct b from Book b order by b.title " );

        List<Book> authors = query.getResultList();

        return authors;
    }

    @Override
    public List<Book> findAllWithPurchasesAndBookCriteriaSortedAPI() {
        EntityManager entityManager = getEntityManager();

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Book> query = criteriaBuilder.createQuery(Book.class);
        query.distinct(Boolean.TRUE);
        Root<Book> root = query.from(Book.class);
        query.orderBy(criteriaBuilder.asc(root.get(Book_.title)));
        Fetch<Book, Purchase> bookPurchaseFetch = root.fetch(Book_.purchases, JoinType.LEFT);
        bookPurchaseFetch.fetch(Purchase_.book, JoinType.LEFT);

        List<Book> authors = entityManager.createQuery(query).getResultList();

        return authors;
    }

    @Override
    @Transactional
    public List<Book> findAllWithPurchasesAndBookSortedSQL() {
        HibernateEntityManager hibernateEntityManager = getEntityManager().unwrap(HibernateEntityManager.class);
        Session session = hibernateEntityManager.getSession();

        org.hibernate.Query query = session.createSQLQuery("select distinct {a.*},{b.*},{p.*} " +
                "from book a " +
                "left join purchase p on a.id=p.book_id " +
                "left join book b on p.book_id=b.id " + "order by a.title")
                .addEntity("a",Book.class)
                .addJoin("p", "a.purchases")
                .addJoin("b", "p.book")
                .addEntity("a",Book.class)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        //todo aici nu stiu exavt cum sa fac, ca nu merge

        List<Book> authors = query.getResultList();


        return authors;

    }
}
