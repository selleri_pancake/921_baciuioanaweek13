package ro.ubb.catalog.core.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import ro.ubb.catalog.core.model.Client;

import java.util.List;

public interface ClientRepository extends CatalogRepository<Client, Long>,ClientRepositoryCustom {

    @Query("select distinct c from Client c")
    @EntityGraph(value = "clientWithPurchasesAndBook", type =
            EntityGraph.EntityGraphType.LOAD)
    List<Client> findAllWithPurchasesAndBook();

    @Query("select distinct c from Client c order by c.full_name")
    @EntityGraph(value = "clientWithPurchasesAndBook", type =
            EntityGraph.EntityGraphType.LOAD)
    List<Client> findAllWithPurchasesAndBookOOrderByFull_name();
}
