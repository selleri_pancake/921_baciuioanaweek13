package ro.ubb.catalog.core.repository;

import ro.ubb.catalog.core.model.Client;

import java.util.List;

public interface ClientRepositoryCustom {
    List<Client> findAllWithPurchasesAndBookJPQL();
    List<Client> findAllWithPurchasesAndBookCriteriaAPI();
    List<Client> findAllWithPurchasesAndBookSQL();

    List<Client> findAllWithPurchasesAndBookSortedJPQL();
    List<Client> findAllWithPurchasesAndBookSortedCriteriaAPI();
    List<Client> findAllWithPurchasesAndBookSortedSQL();
}
