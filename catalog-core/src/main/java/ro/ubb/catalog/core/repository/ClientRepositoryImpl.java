package ro.ubb.catalog.core.repository;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.jpa.HibernateEntityManager;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.catalog.core.model.Client;
import ro.ubb.catalog.core.model.Client_;
import ro.ubb.catalog.core.model.Purchase;
import ro.ubb.catalog.core.model.Purchase_;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.*;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ClientRepositoryImpl extends CustomRepositorySupport implements ClientRepositoryCustom {
    @Override
    public List<ro.ubb.catalog.core.model.Client> findAllWithPurchasesAndBookJPQL() {
        EntityManager entityManager = getEntityManager();
        Query query = entityManager.createQuery(
                "select distinct c from Client c " +
                        "left join fetch c.purchases p " +
                        "left join fetch p.book");
        List<Client> authors = query.getResultList();

        return authors;    }

    @Override
    public List<Client> findAllWithPurchasesAndBookCriteriaAPI() {
        EntityManager entityManager = getEntityManager();

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Client> query = criteriaBuilder.createQuery(Client.class);
        query.distinct(Boolean.TRUE);
        Root<Client> root = query.from(Client.class);
//        query.select(root);
        Fetch<Client, Purchase> clientPurchaseFetch = root.fetch(Client_.purchases, JoinType.LEFT);
        clientPurchaseFetch.fetch(Purchase_.book, JoinType.LEFT);

        List<Client> authors = entityManager.createQuery(query).getResultList();

        return authors;
    }

    @Override
    @Transactional
    public List<Client> findAllWithPurchasesAndBookSQL() {
        HibernateEntityManager hibernateEntityManager = getEntityManager().unwrap(HibernateEntityManager.class);
        Session session = hibernateEntityManager.getSession();

        org.hibernate.Query query = session.createSQLQuery("select distinct {c.*},{p.*},{b.*} " +
                "from client c " +
                "left join purchase p on c.id=p.client_id " +
                "left join book b on p.book_id=b.id ")
                .addEntity("c",Client.class)
                .addJoin("p", "c.purchases")
                .addJoin("b", "p.book")
                .addEntity("c",Client.class)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        List<Client> authors = query.getResultList();


        return authors;    }

    @Override
    public List<Client> findAllWithPurchasesAndBookSortedJPQL() {
        EntityManager entityManager = getEntityManager();
        Query query = entityManager.createQuery(
                "select distinct c from Client c " +
                        "left join fetch c.purchases p " +
                        "left join fetch p.book order by c.full_name");
        List<Client> authors = query.getResultList();

        return authors;
    }

    @Override
    public List<Client> findAllWithPurchasesAndBookSortedCriteriaAPI() {
        EntityManager entityManager = getEntityManager();

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Client> query = criteriaBuilder.createQuery(Client.class);
        query.distinct(Boolean.TRUE);
        Root<Client> root = query.from(Client.class);
//        query.select(root);
       // query.orderBy((List<Order>) root.get(Client_.full_name));
        Fetch<Client, Purchase> clientPurchaseFetch = root.fetch(Client_.purchases, JoinType.LEFT);
        clientPurchaseFetch.fetch(Purchase_.book, JoinType.LEFT);

        List<Client> authors = entityManager.createQuery(query).getResultList();
        authors.sort(Comparator.comparing(Client::getFull_name));
        return authors;
    }

    @Override
    @Transactional
    public List<Client> findAllWithPurchasesAndBookSortedSQL() {
        HibernateEntityManager hibernateEntityManager = getEntityManager().unwrap(HibernateEntityManager.class);
        Session session = hibernateEntityManager.getSession();

        org.hibernate.Query query = session.createSQLQuery("select distinct {c.*},{p.*},{b.*} " +
                "from client c " +
                "left join purchase p on c.id=p.client_id " +
                "left join book b on p.book_id=b.id order by c.full_name")
                .addEntity("c",Client.class)
                .addJoin("p", "c.purchases")
                .addJoin("b", "p.book")
                .addEntity("c",Client.class)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        List<Client> authors = query.getResultList();


        return authors;    }
}
