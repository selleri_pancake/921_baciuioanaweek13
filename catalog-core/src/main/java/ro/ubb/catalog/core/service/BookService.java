package ro.ubb.catalog.core.service;

import ro.ubb.catalog.core.model.Book;

import java.util.List;

/**
 * Created by radu.
 */
public interface BookService {
    List<Book> findAll();
    List<Book> findAllSortedByTitle();
    Book addBook(Book book);
    Book updateBook(Long id, Book book);
    void deleteBook(Long id);
}
