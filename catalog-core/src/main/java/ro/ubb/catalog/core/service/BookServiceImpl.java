package ro.ubb.catalog.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.core.repository.BookRepository;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

/**
 * Created by radu.
 */
@Service
public class BookServiceImpl implements BookService {

    private static final Logger log = LoggerFactory.getLogger(BookServiceImpl.class);

    @Autowired
    private BookRepository bookRepository;


    @Override
    public List<Book> findAll() {
        log.trace("findAll --- method entered");

        File myObj = new File("C:\\Users\\papuci\\Documents\\springAttemptAgain\\catalog-grades\\catalog-core\\src\\main\\java\\ro\\ubb\\catalog\\core\\service\\config.txt");

        try {
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                //System.out.println(data);
                log.trace("AVEM VALOARE DIN FILE: {}", data);
                if(data.equals("JPQL"))
                    return bookRepository.findAllWithPurchasesAndBookJPQL();
                else if(data.equals("CRITERIA")){
                    log.trace("AM AJUNS DIN FILE UL FANCY LA CRITERIA");
                    return bookRepository.findAllWithPurchasesAndBookCriteriaAPI();}
                else if(data.equals("SQL"))
                    return bookRepository.findAllWithPurchasesAndBookSQL();
                else
                    return bookRepository.findAllWithPurchasesAndBook();
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        List<Book> books = bookRepository.findAllWithPurchasesAndBookSQL();

        log.trace("findAll: disciplines={}", books);

        return books;
    }

    @Override
    public List<Book> findAllSortedByTitle() {
        log.trace("findAll --- method entered");

        File myObj = new File("C:\\Users\\papuci\\Documents\\springAttemptAgain\\catalog-grades\\catalog-core\\src\\main\\java\\ro\\ubb\\catalog\\core\\service\\config.txt");

        try {
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                //System.out.println(data);
                log.trace("AVEM VALOARE DIN FILE: {}", data);
                if (data.equals("JPQL"))
                    return bookRepository.findAllWithPurchasesAndBookSortedJPQL();
                else if (data.equals("CRITERIA")) {
                    log.trace("AM AJUNS DIN FILE UL FANCY LA CRITERIA");
                    return bookRepository.findAllWithPurchasesAndBookCriteriaSortedAPI();
                } else if (data.equals("SQL"))
                    return bookRepository.findAllWithPurchasesAndBookSortedSQL();
                else
                    return bookRepository.findAllWithPurchasesAndBookOrderByTitle();
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        //  log.trace("findAll: disciplines={}", books);

        //  return books;
        return bookRepository.findAllWithPurchasesAndBookOrderByTitle();

    }

    @Override
    public Book addBook(Book book) {
        log.trace("addBook: method entered: {}", book);
        Integer i = book.getTitle().hashCode();
        book.setIBAN(i.toString());
        Book save = bookRepository.save(book);
        log.trace("addBook: method finished: {}", save);

        return save;
    }

    @Override
    @Transactional
    public Book updateBook(Long id, Book book) {
        log.trace("updateBook:method entered - id={}, book={}", id, book);
        Optional<Book> optionalBook = bookRepository.findById(id);
        optionalBook.ifPresent(b->{
                    b.setTitle(book.getTitle());
                    b.setPrice(book.getPrice());
                    b.setIBAN(book.getIBAN());
                    b.setAuthor(book.getAuthor());
                }

        );
        Book book1 = optionalBook.orElse(null);
        log.trace("updateBook:method finished - book={}",  book);
        return book1;
    }

    @Override
    public void deleteBook(Long id) {

        log.trace("deleteBook: method entered - id = {}", id);
        //purchaseRepository.findAll().stream().filter(x->x.getBookID().equals(id)).map(x->x.getId()).forEach(x->purchaseRepository.deleteById(x));
        bookRepository.deleteById(id);
        log.trace("delteBook: method finished");
    }
}
