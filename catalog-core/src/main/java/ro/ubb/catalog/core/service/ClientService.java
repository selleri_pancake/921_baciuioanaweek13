package ro.ubb.catalog.core.service;

import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.core.model.Client;
import ro.ubb.catalog.core.model.Purchase;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public interface ClientService {
    Optional<Client> findClient(Long clientId);

    List<Client> findAll();

    Client addClient(Client client);
    Client updateClient(Long id, Client client);
    void deleteClient(Long id);

    List<Purchase> getPurchasesOfClient(Long clientId);
    void addPurchase(Long clientId, Book book);
    Optional<Client> updateClientPurchase(Long studentId, Map<Long, Integer> grades);
    void deletePurchase(Long clientId, Long bookId);

    List<Client> findAllSorted();


}
