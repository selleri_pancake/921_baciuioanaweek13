package ro.ubb.catalog.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.core.model.Client;
import ro.ubb.catalog.core.model.Purchase;
import ro.ubb.catalog.core.repository.BookRepository;
import ro.ubb.catalog.core.repository.ClientRepository;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ClientServiceImpl implements ClientService {
    private static final Logger log = LoggerFactory.getLogger(ClientServiceImpl.class);

    @Autowired
    private ClientRepository clientRepository;

//    @Autowired
//    private ClientRepositoryImpl clientRepositoryImpl;

    @Autowired
    private BookRepository bookRepository;


    @Override
    public Optional<Client> findClient(Long studentId) {
        log.trace("findStudent: studentId={}", studentId);

        Optional<Client> studentOptional = clientRepository.findById(studentId);

        log.trace("findStudent: studentOptional={}", studentOptional);

        return studentOptional;
    }

    @Override
    public List<Client> findAll() {
        log.trace("findAll --- method entered");


        File myObj = new File("C:\\Users\\papuci\\Documents\\springAttemptAgain\\catalog-grades\\catalog-core\\src\\main\\java\\ro\\ubb\\catalog\\core\\service\\config.txt");
        try {
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                //System.out.println(data);
                log.trace("AVEM VALOARE DIN FILE: {}", data);
                if(data.equals("JPQL"))
                    return clientRepository.findAllWithPurchasesAndBookJPQL();
                else if(data.equals("CRITERIA")){
                    log.trace("AM AJUNS DIN FILE UL FANCY LA CRITERIA");
                    return clientRepository.findAllWithPurchasesAndBookCriteriaAPI();}
                else if(data.equals("SQL"))
                    return clientRepository.findAllWithPurchasesAndBookSQL();
                else
                    return clientRepository.findAllWithPurchasesAndBook();
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        List<Client> clients = clientRepository.findAllWithPurchasesAndBook();

        log.trace("findAll: students={}", clients);

        return clients;
    }

    @Override
    public Client addClient(Client client) {
        log.trace("addClient ---- method entered");
        Client save = clientRepository.save(client);
        log.trace("addClient ----- method finished, result = {}", client);
        return client;
    }

    @Override
    @Transactional
    public Client updateClient(Long id, Client client) {

        Optional<Client> clientOptional = clientRepository.findById(id);
        clientOptional.ifPresent(c->{
            c.setFull_name(client.getFull_name());
            c.setAddress(client.getAddress());
        });

        return clientOptional.orElse(null);
    }

    @Override
    public void deleteClient(Long id) {
        // purchaseRepository.findAll().stream().filter(x->x.getClientID().equals(id)).map(x->x.getId()).forEach(x->purchaseRepository.deleteById(x));
        clientRepository.deleteById(id);
    }

    @Override
    @Transactional
    public List<Purchase> getPurchasesOfClient(Long clientId) {
        log.trace("getPurchasesOfClient: method entered, id = {}", clientId);
        List<Client> all = clientRepository.findAll();
        log.trace("getPurchasesOfClient: all = {}", all);

//        for (Client client: all
//             ) {
//            if(client.getId() == clientId)
//            {
//                List<Purchase> purchases = new ArrayList<>(client.getPurchases());
//                return purchases;
//            }
//        }




        Optional<Client> byId = clientRepository.findById(clientId);
        if (byId.isPresent())
        {
            ArrayList<Purchase> purchases = new ArrayList<>(byId.get().getPurchases());
            log.trace("getPurchasesOfClient: result = {}", purchases);
            return purchases;
        }
        return new ArrayList<>();
    }

    @Override
    @Transactional
    public void addPurchase(Long clientId, Book book) {
        log.trace("addPurchase: method entered, clientId = {}, book = {}", clientId, book);
        clientRepository.findAll();
        bookRepository.findAll();
        Optional<Client> byId = clientRepository.findById(clientId);
        Optional<Book> byId1 = bookRepository.findById(book.getId());

        if (byId.isPresent() && byId1.isPresent())
        {
            byId.get().addPurchase(byId1.get(), new Date());
            log.trace("ADDPURCHASE, AFTER AN ADD: {}" ,byId.get().getPurchases());
        }
    }

    @Override
    @Transactional
    public Optional<Client> updateClientPurchase(Long studentId, Map<Long, Integer> grades) {
        log.trace("updateStudentGrades: studentId={}, grades={}", studentId, grades);

        throw new RuntimeException("not yet implemented");
    }

    @Override
    @Transactional
    public void deletePurchase(Long clientId, Long bookId) {
        log.trace("deletePurchase: entered clientID = {}, bookId = {}", clientId, bookId);
        clientRepository.findById(clientId).ifPresent(client -> {
            log.trace("deletePurchase: in func client = {}", client);
            client.deletePurchase(bookId);
            log.trace("deletePurchase: in func purchases = {}", client.getPurchases());
            client.getPurchases().stream().filter(p -> p.getBook().getId().equals(bookId)).findFirst().ifPresent(b -> b.getBook().deletePurchase(clientId));

        });
    }

    @Override
    public List<Client> findAllSorted() {

        log.trace("findAllSorted: method entered");

        File myObj = new File("C:\\Users\\papuci\\Documents\\springAttemptAgain\\catalog-grades\\catalog-core\\src\\main\\java\\ro\\ubb\\catalog\\core\\service\\config.txt");
        try {
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                //System.out.println(data);
                log.trace("AVEM VALOARE DIN FILE: {}", data);
                if(data.equals("JPQL"))
                    return clientRepository.findAllWithPurchasesAndBookSortedJPQL();
                else if(data.equals("CRITERIA")){
                    log.trace("AM AJUNS DIN FILE UL FANCY LA CRITERIA");
                    List<Client> api = clientRepository.findAllWithPurchasesAndBookSortedCriteriaAPI();
                    log.trace("IN GET ALL SORED LA CRITERIA API RESULT: {}", api);
                    return api;}
                else if(data.equals("SQL"))
                    return clientRepository.findAllWithPurchasesAndBookSortedSQL();
                else
                    return clientRepository.findAllWithPurchasesAndBookOOrderByFull_name();
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return clientRepository.findAllWithPurchasesAndBookOOrderByFull_name();
    }


}
