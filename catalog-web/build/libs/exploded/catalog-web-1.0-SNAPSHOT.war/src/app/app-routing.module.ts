import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {StudentsComponent} from "./students/students.component";
import {StudentDetailComponent} from "./students/student-detail/student-detail.component";
import {DisciplinesComponent} from "./disciplines/disciplines.component";
import {StudentNewComponent} from "./students/student-new/student-new.component";
import {BooksComponent} from "./books/books.component";
import {ClientsComponent} from "./clients/clients.component";
import {BookNewComponent} from "./books/book-new/book-new.component";
import {BookDetailComponent} from "./books/book-detail/book-detail.component";
import {ClientDetailComponent} from "./clients/client-detail/client-detail.component";
import {PurchasesComponent} from "./clients/client-detail/purchases/purchases.component";
import {PurchaseNewComponent} from "./clients/client-detail/purchases/purchase-new/purchase-new.component";
import {ClientNewComponent} from "./clients/client-new/client-new.component";


const routes: Routes = [
  // { path: '', redirectTo: '/home', pathMatch: 'full' },
  {path: 'students', component: StudentsComponent},
  {path: 'student/detail/:id', component: StudentDetailComponent},
  {path: 'student/new', component: StudentNewComponent},

  {path: 'disciplines', component: DisciplinesComponent},
  {path: 'books', component: BooksComponent},
  {path: 'clients', component: ClientsComponent},
  {path: 'book/new', component: BookNewComponent},
  {path: 'book/detail/:id', component: BookDetailComponent},
  {path: 'client/detail/:id', component: ClientDetailComponent},
  {path: 'purchases/:id', component: PurchasesComponent},
  {path: 'purchase/new/:id', component: PurchaseNewComponent},
  {path: 'client/new', component: ClientNewComponent},



  // {path: 'enroll', component: DisciplineEnrollComponent},
  // {path: 'grades', component: GradesComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
