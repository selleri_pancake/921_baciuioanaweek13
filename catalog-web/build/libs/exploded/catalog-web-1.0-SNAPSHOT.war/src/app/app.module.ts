import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {AppRoutingModule} from "./app-routing.module";
import {StudentDetailComponent} from "./students/student-detail/student-detail.component";
import {StudentsComponent} from "./students/students.component";
import {StudentListComponent} from "./students/student-list/student-list.component";
import {StudentService} from "./students/shared/student.service";
import {DisciplinesComponent} from "./disciplines/disciplines.component";
import {DisciplineService} from "./disciplines/shared/discipline.service";
import {StudentNewComponent} from "./students/student-new/student-new.component";
import {DisciplineListComponent} from "./disciplines/discipline-list/discipline-list.component";
import { BooksComponent } from './books/books.component';
import { BookListComponent } from './books/book-list/book-list.component';
import {BookService} from "./books/shared/book.service";
import { BookDetailComponent } from './books/book-detail/book-detail.component';
import { ClientsComponent } from './clients/clients.component';
import { ClientListComponent } from './clients/client-list/client-list.component';
import {ClientService} from "./clients/shared/client.service";
import { BookNewComponent } from './books/book-new/book-new.component';
import { ClientDetailComponent } from './clients/client-detail/client-detail.component';
import { PurchasesComponent } from './clients/client-detail/purchases/purchases.component';
import {PurchaseService} from "./clients/client-detail/purchases/shared/purchase.service";
import { PurchaseNewComponent } from './clients/client-detail/purchases/purchase-new/purchase-new.component';
import { ClientNewComponent } from './clients/client-new/client-new.component';


@NgModule({
  declarations: [
    AppComponent,
    StudentDetailComponent,
    StudentsComponent,
    StudentListComponent,
    StudentNewComponent,

    DisciplinesComponent,
    DisciplineListComponent,
    BooksComponent,
    BookListComponent,
    BookDetailComponent,
    ClientsComponent,
    ClientListComponent,
    BookNewComponent,
    ClientDetailComponent,
    PurchasesComponent,
    PurchaseNewComponent,
    ClientNewComponent,


  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
  ],
  providers: [StudentService, DisciplineService, BookService, ClientService, PurchaseService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
