import { Component, OnInit } from '@angular/core';
import {Book} from "../shared/book.model";
import {BookService} from "../shared/book.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {
  books: Book[];
  selectedBook: Book;
  sortedBooks: Book[];
  constructor(private bookService: BookService, private router: Router) { }


  ngOnInit() {
    this.bookService.getBooks().subscribe(books => this.books = books);
  }

  onSelect(book: Book) {
    this.selectedBook = book;
  }

  gotoDetail() {
    this.router.navigate(['/book/detail', this.selectedBook.id]);

  }

  sorted() {
    this.bookService.getBooksSorted().subscribe(result => this.sortedBooks = result);
  }
}
