export class Book {
  id: number;
  title: string;
  author: string;
  IBAN: string;
  price: number;
}
