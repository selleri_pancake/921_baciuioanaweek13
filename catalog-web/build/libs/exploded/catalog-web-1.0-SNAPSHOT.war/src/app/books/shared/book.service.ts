import {Injectable} from '@angular/core';

import {HttpClient} from '@angular/common/http';

import {Book} from './book.model';

import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';


@Injectable()
export class BookService {
  private booksUrl = 'http://localhost:8080/api/books';

  constructor(private httpClient: HttpClient) {
  }

  getBooks(): Observable<Book[]> {
    const observable = this.httpClient
      .get<Array<Book>>(this.booksUrl);
    console.log('in bookservice');
    console.log(observable);
    return observable;
  }

  getBook(id: number): Observable<Book> {
    console.log('the id is: ');
    console.log(id);
    return this.getBooks()
      .pipe(
        // tslint:disable-next-line:triple-equals
        map(books => books.find(book => { console.log(book); console.log(book.id == id); return book.id == id;  }))
      );
  }

  update(book): Observable<Book> {
    const url = `${this.booksUrl}/${book.id}`;
    return this.httpClient
      .put<Book>(url, book);
  }

  saveBook(book: Book): Observable<Book> {
    console.log(book);
    const url = `${this.booksUrl}`;
    const objectObservable = this.httpClient.post<Book>(url, book);
    console.log(objectObservable);
    return objectObservable;
  }

  deleteBook(id: number): Observable<Book> {
    const url = `${this.booksUrl}/${id}`;
    console.log('here I am');
    console.log(url);
    console.log(id);
    // @ts-ignore
    const observable = this.httpClient.delete<Book>(url);
    return observable;
  }
  getBooksSorted(): Observable<Book[]> {
    const url = `${this.booksUrl}/sorted`;
    return this.httpClient.get<Array<Book>>(url);
  }

  getPage(page: number): Observable<Book[]> {
    const url = `${this.booksUrl}/paging/${page}`;
    const observable = this.httpClient.get<Array<Book>>(url);
    return observable;
  }
  //
  // getSortedPage(page: number, sort: string): Observable<Book[]>{
  //   const url = `${this.booksUrl}/paging/sorting/${page}/${sort}`;
  //   const observable = this.httpClient.get<Array<Book>>(url);
  //   return observable;
  // }
  //
  // getPageFilteredByPrice(page: number, price: number): Observable<Book[]> {
  //   const url = `${this.booksUrl}/paging/fbyprice/${price}/${page}`;
  //   return this.httpClient.get<Array<Book>>(url);
  // }
  // getPageFilteredByAuthor(page: number, author: string): Observable<Book[]> {
  //   const url = `${this.booksUrl}/paging/fbyauthor/${author}/${page}`;
  //   return this.httpClient.get<Array<Book>>(url);
  // }
  //
  // getSizeOfFilteredbyPrice(page: number, price: number): Observable<number>{
  //   const url =  `${this.booksUrl}/paging/fbyprice/size/${price}/${page}`;
  //   return this.httpClient.get<number>(url);
  // }
  // getSizeOfFilteredbyAuthor(page: number, author: string): Observable<number> {
  //   const url = `${this.booksUrl}/paging/fbyauthor/size/${author}/${page}`;
  //   return this.httpClient.get<number>(url);
  // }

}
