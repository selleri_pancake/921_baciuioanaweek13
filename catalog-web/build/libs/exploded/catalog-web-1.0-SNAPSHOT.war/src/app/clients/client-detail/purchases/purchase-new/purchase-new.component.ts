import { Component, OnInit } from '@angular/core';
import {Book} from "../../../../books/shared/book.model";
import {BookService} from "../../../../books/shared/book.service";
import {PurchaseService} from "../shared/purchase.service";
import {Location} from "@angular/common";
import {ActivatedRoute, Params} from "@angular/router";
import {switchMap} from "rxjs/operators";

@Component({
  selector: 'app-purchase-new',
  templateUrl: './purchase-new.component.html',
  styleUrls: ['./purchase-new.component.css']
})
export class PurchaseNewComponent implements OnInit {

  books: Book[];
  selectedBook: Book;
  clientId: number;
  constructor(
    private bookService: BookService,
    private purchaseService: PurchaseService,
    private route: ActivatedRoute,
    private location: Location
  ) { }

  ngOnInit() {
    this.bookService.getBooks().subscribe(books => this.books = books);

    this.route.params.pipe(switchMap((params: Params) => {console.log(params['id']); return params['id']; })).subscribe(par =>
    {
      const p = localStorage.getItem('clientID');
      console.log(p);
      console.log(+p);
      this.clientId = +p;

    });
  }

  bookClick(book: Book) {
    console.log(this.clientId);
    this.selectedBook = book;
  }

  savePurchase() {
    this.purchaseService.save(this.clientId, this.selectedBook).subscribe(p => console.log(p));
  }

  goBack() {
      this.location.back();
  }
}
