import {Component, Input, OnInit} from '@angular/core';
import {Client} from '../../shared/client.model';
import {ClientService} from '../../shared/client.service';
import {Location} from '@angular/common';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {switchMap} from 'rxjs/operators';
import {Purchase} from './shared/purchase.model';
import {PurchaseService} from './shared/purchase.service';

@Component({
  selector: 'app-purchases',
  templateUrl: './purchases.component.html',
  styleUrls: ['./purchases.component.css']
})
export class PurchasesComponent implements OnInit {

  purchases: Purchase[];
  client: Client;
  selectedPurchase: Purchase;
  constructor(
    private clientService: ClientService,
    private purchaseService: PurchaseService,
    private location: Location,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.route.params
      .pipe(switchMap((params: Params) => this.clientService.getClient(+params['id'])))
      .subscribe(client => {this.client = client; console.log(this.client); });

    this.route.params.pipe(switchMap((params: Params) => this.purchaseService.getPurchases(+params['id'])))
      .subscribe(purchases => this.purchases = purchases);
  }

  gotoDetail() {

  }

  onClick(purchase: Purchase) {

  }

  gotoNew() {
    console.log(this.client.id);
    localStorage.setItem('clientID', this.client.id.toString());
    this.router.navigate(['purchase/new', this.client.id]);
  }

  delete(purchase: Purchase) {
      const bookId = purchase.bookId;
      const clientId = purchase.clientId;
      this.purchaseService.deletePurchase(bookId, clientId).subscribe();
  }
}
