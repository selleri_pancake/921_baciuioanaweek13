import {Book} from "../../../../books/shared/book.model";
import {Client} from "../../../shared/client.model";


export class Purchase {
  bookId: number;
  clientId: number;
  bookTitle: string;
  clientName: string;
  bookAuthor: string;
  date: Date;
}
