import {Injectable} from '@angular/core';

import {HttpClient} from '@angular/common/http';

// tslint:disable-next-line:import-blacklist
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Purchase} from './purchase.model';
import {Book} from "../../../../books/shared/book.model";


@Injectable()
export class PurchaseService {
  private purchasesUrl = 'http://localhost:8080/api/purchases';

  constructor(private httpClient: HttpClient) {
  }

  getPurchases(clientId: number): Observable<Purchase[]> {
    const url = `${this.purchasesUrl}/${clientId}`;
    return this.httpClient
      .get<Array<Purchase>>(url);
  }

  getPurchase(bookid: number, clientid: number): Observable<Purchase> {
    return this.getPurchases(clientid)
      .pipe(
        map(purchases => purchases.find(purchase => purchase.bookId === bookid))
      );
  }

  update(purchase): Observable<Purchase> {
    const url = `${this.purchasesUrl}/${purchase.id}`;
    return this.httpClient
      .put<Purchase>(url, purchase);
  }

  save(clientId: number, book: Book): Observable<any> {
    const url = `${this.purchasesUrl}/${clientId}`;
    return this.httpClient.post<Purchase>(url, book);
  }

  deletePurchase(bookId: number, clientId: number): Observable<Purchase> {
    const url = `${this.purchasesUrl}/delete/${clientId}/${bookId}`;
    return this.httpClient.delete<Purchase>(url);
  }
}
