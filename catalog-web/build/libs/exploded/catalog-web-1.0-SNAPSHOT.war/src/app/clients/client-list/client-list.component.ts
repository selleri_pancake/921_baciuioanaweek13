import { Component, OnInit } from '@angular/core';
import {Client} from "../shared/client.model";
import {ClientService} from "../shared/client.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.css']
})
export class ClientListComponent implements OnInit {
  clients: Client[];
  selectedClient: Client;

  constructor(private clientService: ClientService, private router: Router) { }

  ngOnInit() {
    this.clientService.getClients().subscribe(clients => this.clients = clients);
  }

  onSelect(client: Client) {
    this.selectedClient = client;
  }

  gotoDetail() {
    this.router.navigate(['client/detail', this.selectedClient.id]);
  }
}
