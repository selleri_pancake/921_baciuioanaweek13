import { Component, OnInit } from '@angular/core';
import {ClientService} from '../shared/client.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-client-new',
  templateUrl: './client-new.component.html',
  styleUrls: ['./client-new.component.css']
})
export class ClientNewComponent implements OnInit {

  constructor(private clientService: ClientService, private location: Location) { }

  ngOnInit() {
  }

  saveClient(fullname: string, address: string) {
    this.clientService.save({id: null, full_name: fullname, address: address}).subscribe(client => console.log(client));
  }

  goBack() {
    this.location.back();
  }
}
