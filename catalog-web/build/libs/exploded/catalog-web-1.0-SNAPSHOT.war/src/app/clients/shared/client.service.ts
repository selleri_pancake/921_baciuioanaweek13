import {Injectable} from '@angular/core';

import {HttpClient} from '@angular/common/http';

import {Client} from './client.model';

import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Book} from "../../books/shared/book.model";


@Injectable()
export class ClientService {
  private clientsUrl = 'http://localhost:8080/api/clients';

  constructor(private httpClient: HttpClient) {
  }

  getClients(): Observable<Client[]> {
    return this.httpClient
      .get<Array<Client>>(this.clientsUrl);
  }

  getClient(id: number): Observable<Client> {
    return this.getClients()
      .pipe(
        // tslint:disable-next-line:triple-equals
        map(clients => clients.find(client => client.id == id))
      );
  }

  update(client): Observable<Client> {
    const url = `${this.clientsUrl}/${client.id}`;
    return this.httpClient
      .put<Client>(url, client);
  }

  save(client): Observable<Client> {
    const url = `${this.clientsUrl}`;
    const objectObservable = this.httpClient.post<Client>(url, client);
    return objectObservable;
  }

  deleteClient(id: number): Observable<Client> {
    const url = `${this.clientsUrl}/${id}`;
    // @ts-ignore
    return this.httpClient.delete<Client>(url);
  }}
