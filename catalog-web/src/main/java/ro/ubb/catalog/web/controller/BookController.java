package ro.ubb.catalog.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.core.service.BookService;
import ro.ubb.catalog.web.converter.BookConverter;
import ro.ubb.catalog.web.dto.BookDto;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by radu.
 */
@RestController
public class BookController {

    private static final Logger log = LoggerFactory.getLogger(BookController.class);

    @Autowired
    private BookService bookService;

    @Autowired
    private BookConverter bookConverter;


    @RequestMapping(value = "/books", method = RequestMethod.GET)
    public Set<BookDto> getBooks() {
        log.trace("getBooks --- method entered");

        List<Book> books = bookService.findAll();

        Set<BookDto> bookDtos =
                new HashSet<>(bookConverter.convertModelsToDtos(books));

        log.trace("getBooks: books={}", bookDtos);

        return bookDtos;
    }

    @RequestMapping(value = "/books/sorted", method = RequestMethod.GET)
    public List<BookDto> getBooksByTitle() {
        log.trace("getBooks --- method entered");

        List<Book> books = bookService.findAllSortedByTitle();

        List<BookDto> bookDtos =
                new ArrayList<>(bookConverter.convertModelsToDtos(books));

        log.trace("getBooks: books={}", bookDtos);

        return bookDtos;
    }

    @RequestMapping(value = "/books", method = RequestMethod.POST)
    BookDto addBook(@RequestBody BookDto bookDto)
    {
        log.trace("addBook ----- entered");
        log.trace("the bookDTO: {}", bookDto);
        Book bk = bookConverter.convertDtoToModel(bookDto);
        log.trace("the book that came from converter: {}", bk);
        BookDto book = bookConverter.convertModelToDto(bookService.addBook(bookConverter.convertDtoToModel(bookDto)));
        log.trace("addBook ----- finished, result = {}",book);
        return book;

    }

    @RequestMapping(value = "/books/{bookId}", method = RequestMethod.PUT)
    public BookDto updateBook(
            @PathVariable final Long bookId,
            @RequestBody final BookDto bookDto) {
        log.trace("updateBook: bookId={}, bookDtoMap={}", bookId, bookDto);

//        Student student = studentService.updateStudent(studentId,
//                studentDto.getSerialNumber(),
//                studentDto.getName(), studentDto.getGroupNumber());
        BookDto book = bookConverter.convertModelToDto(bookService.updateBook(bookId, bookConverter.convertDtoToModel(bookDto)));

        // StudentDto result = studentConverter.convertModelToDto(student);

        log.trace("updateStudent: result={}", book);

        return book;
    }

    @RequestMapping(value = "/books/{bookId}", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteBook(@PathVariable Long bookId)
    {
        log.trace("deleteBook --- entered, id = {}", bookId);
        bookService.deleteBook(bookId);
        log.trace("deleteBook --- finished");
        return new ResponseEntity<>(HttpStatus.OK);
    }




}
