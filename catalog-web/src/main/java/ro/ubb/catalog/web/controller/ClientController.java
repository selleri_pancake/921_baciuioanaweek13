package ro.ubb.catalog.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.catalog.core.model.Client;
import ro.ubb.catalog.core.service.ClientService;
import ro.ubb.catalog.web.converter.ClientConverter;
import ro.ubb.catalog.web.dto.ClientDto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by radu.
 */

@RestController
public class ClientController {

    private static final Logger log = LoggerFactory.getLogger(ClientController.class);

    @Autowired
    private ClientService clientService;

    @Autowired
    private ClientConverter clientConverter;


    @RequestMapping(value = "/clients", method = RequestMethod.GET)
    public List<ClientDto> getStudents() {
        log.trace("getClients");

        List<Client> clients = clientService.findAll();

        log.trace("getClients: students={}", clients);

        return new ArrayList<>(clientConverter.convertModelsToDtos(clients));
    }

    @RequestMapping(value = "/clients/{clientId}", method = RequestMethod.PUT)
    public ClientDto updateStudent(
            @PathVariable final Long clientId,
            @RequestBody final ClientDto clientDto) {

        log.trace("updateClient: clientId={}, clientDtoMap={}", clientId, clientDto);
        ClientDto client = clientConverter.convertModelToDto(clientService.updateClient(clientId, clientConverter.convertDtoToModel(clientDto)));
        // StudentDto result = studentConverter.convertModelToDto(student);

        log.trace("updateClient: result={}", client);

        return client;
    }

    @RequestMapping(value = "/clients", method = RequestMethod.POST)
    public ClientDto createStudent(
            @RequestBody final ClientDto clientDto) {
        log.trace("addClient ----- entered");
        ClientDto client = clientConverter.convertModelToDto(clientService.addClient(clientConverter.convertDtoToModel(clientDto)));
        log.trace("addClient ----- finished, result = {}",client);
        return client;

    }

    @RequestMapping(value = "clients/{clientId}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteStudent(@PathVariable final Long clientId) {
        log.trace("deleteClient --- entered, id = {}", clientId);
        clientService.deleteClient(clientId);
        log.trace("deleteClient --- finished");
        return new ResponseEntity<>(HttpStatus.OK);
    }





}
