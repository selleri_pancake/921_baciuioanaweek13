package ro.ubb.catalog.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.catalog.core.service.ClientService;
import ro.ubb.catalog.web.converter.BookConverter;
import ro.ubb.catalog.web.converter.PurchaseConverter;
import ro.ubb.catalog.web.dto.BookDto;
import ro.ubb.catalog.web.dto.PurchaseDto;

import java.util.*;

/**
 * Created by radu.
 */

@RestController
public class PurchaseController {
    private static final Logger log = LoggerFactory.getLogger(PurchaseController.class);

    @Autowired
    private ClientService clientService;

    @Autowired
    private PurchaseConverter purchaseConverter;
    @Autowired
    private BookConverter bookConverter;


    @RequestMapping(value = "/purchases/{clientId}", method = RequestMethod.GET)
    public ArrayList<PurchaseDto> getPurchases(
            @PathVariable final Long clientId) {
        log.trace("getPurchases, method entered: studentId={}", clientId);
        ArrayList<PurchaseDto> purchaseDtos = new ArrayList<>(purchaseConverter.convertModelsToDtos(clientService.getPurchasesOfClient(clientId)));
        log.trace("getPurchases, method finished: result={}",purchaseDtos);
        log.trace("AM AJUNS AICI AM AVANSAT O TZAR");
        return purchaseDtos;
    }

    @RequestMapping(value = "/purchases/{clientId}", method = RequestMethod.POST)
    public ResponseEntity<?> addPurchase(@PathVariable Long clientId, @RequestBody BookDto bookDto)
    {
        log.trace("addPurchase: method entered, clientId: {}, purchase: {}", clientId, bookDto);
        clientService.addPurchase(clientId, bookConverter.convertDtoToModel(bookDto));
        return new ResponseEntity<>(HttpStatus.OK);

    }

    @RequestMapping(value = "/grades/{studentId}", method = RequestMethod.PUT)
    public Set<PurchaseDto> updateStudentGrades(
            @PathVariable final Long studentId,
            @RequestBody final Set<PurchaseDto> purchaseDtos) {
        log.trace("updateStudentGrades: studentId={}, studentDisciplineDtos={}",
                studentId, purchaseDtos);

        throw new RuntimeException("not yet implemented");
    }

    @RequestMapping(value = "purchases/delete/{clientId}/{bookId}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deletePurchase(@PathVariable Long clientId, @PathVariable Long bookId)
    {
        log.trace("deletePurchase: entered, clientId = {}, bookId = {}", clientId, bookId);
        clientService.deletePurchase(clientId, bookId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
