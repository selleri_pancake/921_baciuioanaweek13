package ro.ubb.catalog.web.converter;

import org.springframework.stereotype.Component;
import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.web.dto.BookDto;

/**
 * Created by radu.
 */
@Component
public class BookConverter extends AbstractConverterBaseEntityConverter<Book, BookDto> {
    @Override
    public Book convertDtoToModel(BookDto book) {

        Book dto = Book.builder().author(book.getAuthor()).IBAN(book.getIBAN()).price(book.getPrice()).title(book.getTitle()).build();
        dto.setId(book.getId());
        return dto;
    }

    @Override
    public BookDto convertModelToDto(Book book) {
        BookDto dto = BookDto.builder().author(book.getAuthor()).IBAN(book.getIBAN()).price(book.getPrice()).title(book.getTitle()).build();
        dto.setId(book.getId());
        return dto;
    }
}
