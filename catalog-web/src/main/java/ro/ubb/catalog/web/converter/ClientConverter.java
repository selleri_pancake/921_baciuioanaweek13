package ro.ubb.catalog.web.converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import ro.ubb.catalog.core.model.BaseEntity;
import ro.ubb.catalog.core.model.Client;
import ro.ubb.catalog.web.dto.ClientDto;

import java.util.stream.Collectors;

/**
 * Created by radu.
 */

@Component
public class ClientConverter extends AbstractConverterBaseEntityConverter<Client, ClientDto> {

    private static final Logger log = LoggerFactory.getLogger(ClientConverter.class);

    @Override
    public Client convertDtoToModel(ClientDto client) {

        Client clientDto = Client.builder().full_name(client.getFull_name()).address(client.getAddress()).build();
        clientDto.setId(client.getId());
        return clientDto;    }

    @Override
    public ClientDto convertModelToDto(Client client) {
        ClientDto clientDto = ClientDto.builder().full_name(client.getFull_name()).address(client.getAddress()).build();
        clientDto.setId(client.getId());
        return clientDto;
    }
}
