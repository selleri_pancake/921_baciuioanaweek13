package ro.ubb.catalog.web.converter;

import org.springframework.stereotype.Component;
import ro.ubb.catalog.core.model.Purchase;
import ro.ubb.catalog.web.dto.PurchaseDto;

/**
 * Created by radu.
 */
@Component
public class PurchaseConverter
        extends AbstractConverter<Purchase, PurchaseDto> {

    @Override
    public Purchase convertDtoToModel(PurchaseDto purchase) {
//        return Purchase.builder().book(purchase.getBook()).client(purchase.getClient()).date(purchase.getDate())
//                .build();
        return null;
    }

    @Override
    public PurchaseDto convertModelToDto(Purchase purchase) {
        return PurchaseDto.builder().bookTitle(purchase.getBook().getTitle()).clientName(purchase.getClient().getFull_name()).date(purchase.getDate()).bookAuthor(purchase.getBook().getAuthor()).bookId(purchase.getBook().getId()).clientId(purchase.getClient().getId())
                .build();
    }
}
