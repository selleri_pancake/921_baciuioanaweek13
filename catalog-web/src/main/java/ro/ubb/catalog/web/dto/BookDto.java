package ro.ubb.catalog.web.dto;

import lombok.*;

/**
 * Created by radu.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class BookDto extends BaseDto {
    private String IBAN;
    private String author;
    private String title;
    private Double price;

    @Override
    public String toString() {
        return "BookDto{" +
                "IBAN='" + IBAN + '\'' +
                ", author='" + author + '\'' +
                ", title='" + title + '\'' +
                ", price=" + price +
                '}';
    }
}
