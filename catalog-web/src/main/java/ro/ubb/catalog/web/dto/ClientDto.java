package ro.ubb.catalog.web.dto;

import lombok.*;

import java.util.Set;

/**
 * Created by radu.
 */

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class ClientDto extends BaseDto {
    private String full_name;
    private String address;
}
