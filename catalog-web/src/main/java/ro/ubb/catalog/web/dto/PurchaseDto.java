package ro.ubb.catalog.web.dto;

import lombok.*;
import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.core.model.Client;

import java.util.Date;

/**
 * Created by radu.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class PurchaseDto {
    private Long bookId;
    private Long clientId;
    private String bookTitle;
    private String clientName;
    private String bookAuthor;
    private Date date;
}
